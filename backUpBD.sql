USE [master]
GO
/****** Object:  Database [DBORDEN]    Script Date: 06/08/2020 12:28:55 ******/
CREATE DATABASE [DBORDEN]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ORDENDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQL2019\MSSQL\DATA\ORDENDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ORDENDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQL2019\MSSQL\DATA\ORDENDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [DBORDEN] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBORDEN].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBORDEN] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBORDEN] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBORDEN] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBORDEN] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBORDEN] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBORDEN] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBORDEN] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBORDEN] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBORDEN] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBORDEN] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBORDEN] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBORDEN] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBORDEN] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBORDEN] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBORDEN] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBORDEN] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBORDEN] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBORDEN] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBORDEN] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBORDEN] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBORDEN] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBORDEN] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBORDEN] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DBORDEN] SET  MULTI_USER 
GO
ALTER DATABASE [DBORDEN] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBORDEN] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBORDEN] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBORDEN] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DBORDEN] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DBORDEN] SET QUERY_STORE = OFF
GO
USE [DBORDEN]
GO
/****** Object:  Table [dbo].[Asesor]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asesor](
	[idAsesor] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](200) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Asesor] PRIMARY KEY CLUSTERED 
(
	[idAsesor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bodega]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bodega](
	[idBodega] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](400) NOT NULL,
	[descripcion] [nvarchar](400) NULL,
 CONSTRAINT [PK_bodega] PRIMARY KEY CLUSTERED 
(
	[idBodega] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BodegaProducto]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BodegaProducto](
	[idBodegaProducto] [int] IDENTITY(1,1) NOT NULL,
	[idBodega] [int] NOT NULL,
	[referencia] [nvarchar](50) NOT NULL,
	[existencia] [int] NOT NULL,
	[idProducto] [int] NOT NULL,
 CONSTRAINT [PK_BodegaProducto] PRIMARY KEY CLUSTERED 
(
	[idBodegaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categoria](
	[idCategoria] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](400) NOT NULL,
	[descripcion] [nvarchar](400) NULL,
 CONSTRAINT [PK_categoria] PRIMARY KEY CLUSTERED 
(
	[idCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ciudad]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ciudad](
	[idCiudad] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Ciudad] PRIMARY KEY CLUSTERED 
(
	[idCiudad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[nitcc] [nvarchar](20) NOT NULL,
	[nombre] [nvarchar](200) NOT NULL,
	[direccion] [nvarchar](200) NOT NULL,
	[telefono1] [nvarchar](50) NOT NULL,
	[telefono2] [nvarchar](50) NULL,
	[correo] [nvarchar](200) NULL,
	[contacto] [nvarchar](100) NULL,
	[correoContacto] [nvarchar](200) NULL,
	[telContacto] [nvarchar](50) NULL,
	[ciudad] [nvarchar](100) NULL,
	[idAsesor] [int] NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Consecutivo]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Consecutivo](
	[numOrden] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleOrden]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleOrden](
	[idDetalle] [int] IDENTITY(1,1) NOT NULL,
	[idProducto] [int] NOT NULL,
	[idBodegaProducto] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[descuento] [decimal](2, 2) NULL,
	[total] [decimal](18, 2) NOT NULL,
	[numOrden] [int] NOT NULL,
	[idOrden] [int] NOT NULL,
 CONSTRAINT [PK_DetalleOrden] PRIMARY KEY CLUSTERED 
(
	[idDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[idMenu] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
 CONSTRAINT [PK_menus] PRIMARY KEY CLUSTERED 
(
	[idMenu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operario]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operario](
	[idOperario] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](200) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Operario] PRIMARY KEY CLUSTERED 
(
	[idOperario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orden]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orden](
	[idOrden] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NOT NULL,
	[fecha] [date] NOT NULL,
	[subTotal] [decimal](18, 2) NOT NULL,
	[total] [decimal](18, 2) NOT NULL,
	[estado] [nchar](10) NOT NULL,
	[numOrden] [int] NOT NULL,
	[cerrada] [bit] NOT NULL,
 CONSTRAINT [PK_Orden] PRIMARY KEY CLUSTERED 
(
	[idOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[idProducto] [int] IDENTITY(1,1) NOT NULL,
	[referencia] [nvarchar](50) NOT NULL,
	[descripcion] [varchar](max) NULL,
	[marca] [nvarchar](100) NULL,
	[unidadMedida] [nvarchar](50) NULL,
	[dimension] [nvarchar](50) NULL,
	[precioCompra] [decimal](18, 2) NULL,
	[precioVenta] [decimal](18, 2) NULL,
	[idCategoria] [int] NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductoConfirmado]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductoConfirmado](
	[idProductoTemporal] [int] IDENTITY(1,1) NOT NULL,
	[idProducto] [int] NOT NULL,
	[idBodegaProducto] [int] NOT NULL,
	[precio] [decimal](18, 0) NOT NULL,
	[cantidad] [int] NOT NULL,
	[descripcion] [nvarchar](200) NOT NULL,
	[idCliente] [int] NOT NULL,
	[dcto] [decimal](18, 0) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_ProductoConfirmado] PRIMARY KEY CLUSTERED 
(
	[idProductoTemporal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[rol] [nvarchar](100) NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol_Vista]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol_Vista](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idRol] [int] NOT NULL,
	[idVista] [int] NOT NULL,
 CONSTRAINT [PK_Rol_Vista] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicio]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servicio](
	[idServicio] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[descripcion] [nchar](500) NULL,
	[precio] [decimal](18, 2) NOT NULL,
	[comision] [decimal](2, 2) NOT NULL,
	[estado] [bit] NOT NULL,
	[cobro] [bit] NOT NULL,
 CONSTRAINT [PK_Servicio] PRIMARY KEY CLUSTERED 
(
	[idServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TemOrden]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemOrden](
	[idTemOrden] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NOT NULL,
	[idProducto] [int] NOT NULL,
	[descripcion] [varchar](max) NULL,
	[idBodegaProducto] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[dcto] [decimal](2, 2) NULL,
	[estado] [bit] NOT NULL,
	[precio] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TemOrden] PRIMARY KEY CLUSTERED 
(
	[idTemOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [nvarchar](50) NULL,
	[nombres] [nvarchar](200) NULL,
	[apellidos] [nvarchar](200) NULL,
	[correo] [nvarchar](200) NULL,
	[idRol] [int] NOT NULL,
	[estado] [bit] NOT NULL,
	[contrasena] [nvarchar](50) NOT NULL,
	[idAsesor] [int] NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vista]    Script Date: 06/08/2020 12:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vista](
	[idVista] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[idMenu] [int] NOT NULL,
 CONSTRAINT [PK_Vista] PRIMARY KEY CLUSTERED 
(
	[idVista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Asesor] ON 

INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (2, N'VALEN', 1)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (3, N'CATALINA', 0)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (4, N'JUAN', 1)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (5, N'FELIPE1', 0)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (7, N'MARCO', 1)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (8, N'CAMILO', 0)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (9, N'FERNANDO', 1)
INSERT [dbo].[Asesor] ([idAsesor], [nombre], [estado]) VALUES (10, N'CARLOS', 1)
SET IDENTITY_INSERT [dbo].[Asesor] OFF
GO
SET IDENTITY_INSERT [dbo].[Bodega] ON 

INSERT [dbo].[Bodega] ([idBodega], [nombre], [descripcion]) VALUES (6, N'0001 SERVI', N'SERVI')
INSERT [dbo].[Bodega] ([idBodega], [nombre], [descripcion]) VALUES (7, N'SABANETA', N'SABANETA')
SET IDENTITY_INSERT [dbo].[Bodega] OFF
GO
SET IDENTITY_INSERT [dbo].[BodegaProducto] ON 

INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (1, 6, N'10165/65R13H308', 10, 1)
INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (2, 7, N'10165/65R13H308', 10, 1)
INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (3, 7, N'295/80R22.5 ', 10, 5)
INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (4, 7, N'10185/65R14H308', 10, 2)
INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (5, 7, N'10165/65R13CS1', 8, 6)
INSERT [dbo].[BodegaProducto] ([idBodegaProducto], [idBodega], [referencia], [existencia], [idProducto]) VALUES (6, 7, N'10165/65R13ROYALBLACK', 5, 8)
SET IDENTITY_INSERT [dbo].[BodegaProducto] OFF
GO
SET IDENTITY_INSERT [dbo].[Categoria] ON 

INSERT [dbo].[Categoria] ([idCategoria], [nombre], [descripcion]) VALUES (2, N'500 LLANTAS NUEVAS ', N'LLANTAS NUEVAS')
INSERT [dbo].[Categoria] ([idCategoria], [nombre], [descripcion]) VALUES (9, N'SERVICIO REENCAUCHE', N'SERVICIO REENCAUCHE')
SET IDENTITY_INSERT [dbo].[Categoria] OFF
GO
SET IDENTITY_INSERT [dbo].[Ciudad] ON 

INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (1, N'BARBOSA')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (2, N'BELLO')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (3, N'CALDAS')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (4, N'COPACABANA')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (5, N'ENVIGADO')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (6, N'GIRARDOTA')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (7, N'ITAGUI')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (8, N'LA ESTRELLA')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (9, N'MEDELLIN')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (10, N'SABANETA')
INSERT [dbo].[Ciudad] ([idCiudad], [nombre]) VALUES (11, N'BUCARAMANGA')
SET IDENTITY_INSERT [dbo].[Ciudad] OFF
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([idCliente], [nitcc], [nombre], [direccion], [telefono1], [telefono2], [correo], [contacto], [correoContacto], [telContacto], [ciudad], [idAsesor]) VALUES (10, N'1111', N'EMPRESA S.A', N'CRA', N'23', NULL, N'a@gmail.com', NULL, NULL, NULL, N'ENVIGADO', 4)
INSERT [dbo].[Cliente] ([idCliente], [nitcc], [nombre], [direccion], [telefono1], [telefono2], [correo], [contacto], [correoContacto], [telContacto], [ciudad], [idAsesor]) VALUES (11, N'2222', N'METROPLUS', N'CRA', N'234', NULL, N'c@gmail.com', NULL, NULL, NULL, N'ITAGUI', 9)
INSERT [dbo].[Cliente] ([idCliente], [nitcc], [nombre], [direccion], [telefono1], [telefono2], [correo], [contacto], [correoContacto], [telContacto], [ciudad], [idAsesor]) VALUES (12, N'3333', N'SERVITRANS', N'CALLE', N'4', NULL, N'a@gmail.com', NULL, NULL, NULL, N'BELLO', 4)
INSERT [dbo].[Cliente] ([idCliente], [nitcc], [nombre], [direccion], [telefono1], [telefono2], [correo], [contacto], [correoContacto], [telContacto], [ciudad], [idAsesor]) VALUES (13, N'5555', N'COOPEBOMBAS', N'CRA', N'5', NULL, N'B@gmail.com', NULL, NULL, NULL, N'ITAGUI', 4)
INSERT [dbo].[Cliente] ([idCliente], [nitcc], [nombre], [direccion], [telefono1], [telefono2], [correo], [contacto], [correoContacto], [telContacto], [ciudad], [idAsesor]) VALUES (14, N'4444', N'prueba2', N'cll', N'3456', NULL, N'n@abc.com', NULL, NULL, NULL, N'BUCARAMANGA', 9)
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
INSERT [dbo].[Consecutivo] ([numOrden]) VALUES (1010)
GO
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([idMenu], [nombre]) VALUES (1, N'Cliente')
INSERT [dbo].[Menu] ([idMenu], [nombre]) VALUES (2, N'Asesor')
SET IDENTITY_INSERT [dbo].[Menu] OFF
GO
SET IDENTITY_INSERT [dbo].[Operario] ON 

INSERT [dbo].[Operario] ([idOperario], [nombre], [estado]) VALUES (2, N'KATALINA', 1)
INSERT [dbo].[Operario] ([idOperario], [nombre], [estado]) VALUES (3, N'VALENTINA', 1)
INSERT [dbo].[Operario] ([idOperario], [nombre], [estado]) VALUES (4, N'OSCAR', 1)
SET IDENTITY_INSERT [dbo].[Operario] OFF
GO
SET IDENTITY_INSERT [dbo].[Producto] ON 

INSERT [dbo].[Producto] ([idProducto], [referencia], [descripcion], [marca], [unidadMedida], [dimension], [precioCompra], [precioVenta], [idCategoria]) VALUES (1, N'10165/65R13H308', N'LLANTA 165/65R13 HANKOOK H308
', N'HANKOOK', N'UND', N'165/65R13', CAST(100.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Producto] ([idProducto], [referencia], [descripcion], [marca], [unidadMedida], [dimension], [precioCompra], [precioVenta], [idCategoria]) VALUES (2, N'10185/65R14H308', N'LLANTA 185/65R14 HANKOOK H308', N'HANKOOK', N'UND', N'185/65R14', CAST(100.00 AS Decimal(18, 2)), CAST(120.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Producto] ([idProducto], [referencia], [descripcion], [marca], [unidadMedida], [dimension], [precioCompra], [precioVenta], [idCategoria]) VALUES (5, N'295/80R22.5 ', N'SERVICO REEN/295/80R22.5 HANSUGI', N'HANSUGI', N'UND', N'295/80R22.5', CAST(50.00 AS Decimal(18, 2)), CAST(60.00 AS Decimal(18, 2)), 9)
INSERT [dbo].[Producto] ([idProducto], [referencia], [descripcion], [marca], [unidadMedida], [dimension], [precioCompra], [precioVenta], [idCategoria]) VALUES (6, N'10165/65R13CS1', N'LLANTA 165/65R13 COOPER CS1', N'COOPER', N'UNI', N'165/65R13', CAST(80.00 AS Decimal(18, 2)), CAST(90.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Producto] ([idProducto], [referencia], [descripcion], [marca], [unidadMedida], [dimension], [precioCompra], [precioVenta], [idCategoria]) VALUES (8, N'10165/65R13ROYALBLACK', N'LLANTA 165/65R13 ROYAL BLACK  COMFORT', N'ROYAL BLACK', N'UND', N'165/65R13', CAST(70.00 AS Decimal(18, 2)), CAST(80.00 AS Decimal(18, 2)), 2)
SET IDENTITY_INSERT [dbo].[Producto] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductoConfirmado] ON 

INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (25, 2, 4, CAST(120 AS Decimal(18, 0)), 0, N'LLANTA 185/65R14 HANKOOK H308', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (26, 2, 4, CAST(120 AS Decimal(18, 0)), 1, N'LLANTA 185/65R14 HANKOOK H308', 10, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (27, 8, 6, CAST(80 AS Decimal(18, 0)), 0, N'LLANTA 165/65R13 ROYAL BLACK  COMFORT', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (28, 8, 6, CAST(80 AS Decimal(18, 0)), 0, N'LLANTA 165/65R13 ROYAL BLACK  COMFORT', 10, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (29, 1, 1, CAST(120 AS Decimal(18, 0)), 1, N'LLANTA 165/65R13 HANKOOK H308
', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (30, 1, 1, CAST(120 AS Decimal(18, 0)), 2, N'LLANTA 165/65R13 HANKOOK H308
', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (31, 2, 4, CAST(120 AS Decimal(18, 0)), 1, N'LLANTA 185/65R14 HANKOOK H308', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (32, 2, 4, CAST(120 AS Decimal(18, 0)), 2, N'LLANTA 185/65R14 HANKOOK H308', 13, CAST(0 AS Decimal(18, 0)), 1)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (33, 6, 5, CAST(90 AS Decimal(18, 0)), 3, N'LLANTA 165/65R13 COOPER CS1', 13, CAST(0 AS Decimal(18, 0)), 0)
INSERT [dbo].[ProductoConfirmado] ([idProductoTemporal], [idProducto], [idBodegaProducto], [precio], [cantidad], [descripcion], [idCliente], [dcto], [estado]) VALUES (34, 2, 4, CAST(120 AS Decimal(18, 0)), 1, N'LLANTA 185/65R14 HANKOOK H308', 13, CAST(0 AS Decimal(18, 0)), 0)
SET IDENTITY_INSERT [dbo].[ProductoConfirmado] OFF
GO
SET IDENTITY_INSERT [dbo].[Rol] ON 

INSERT [dbo].[Rol] ([idRol], [rol]) VALUES (1, N'ASESOR')
SET IDENTITY_INSERT [dbo].[Rol] OFF
GO
SET IDENTITY_INSERT [dbo].[Rol_Vista] ON 

INSERT [dbo].[Rol_Vista] ([id], [idRol], [idVista]) VALUES (1, 1, 1)
INSERT [dbo].[Rol_Vista] ([id], [idRol], [idVista]) VALUES (2, 1, 2)
INSERT [dbo].[Rol_Vista] ([id], [idRol], [idVista]) VALUES (3, 1, 3)
SET IDENTITY_INSERT [dbo].[Rol_Vista] OFF
GO
SET IDENTITY_INSERT [dbo].[TemOrden] ON 

INSERT [dbo].[TemOrden] ([idTemOrden], [idCliente], [idProducto], [descripcion], [idBodegaProducto], [cantidad], [dcto], [estado], [precio]) VALUES (1, 13, 2, N'LLANTA 185/65R14 HANKOOK H308', 4, 1, NULL, 0, CAST(120.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[TemOrden] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([idUsuario], [usuario], [nombres], [apellidos], [correo], [idRol], [estado], [contrasena], [idAsesor]) VALUES (4, N'KAT', N'CATALINA', N'ZAPATA', N'khatalina22@gmail.com', 1, 1, N'a', 4)
INSERT [dbo].[Usuario] ([idUsuario], [usuario], [nombres], [apellidos], [correo], [idRol], [estado], [contrasena], [idAsesor]) VALUES (5, N'VAL', N'VALEN', N'ZAPATA', N'v@gmail.com', 1, 1, N'1', 9)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
SET IDENTITY_INSERT [dbo].[Vista] ON 

INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (1, N'Index', 1)
INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (2, N'Editar', 1)
INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (3, N'Nuevo', 1)
INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (4, N'Index', 2)
INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (5, N'Editar', 2)
INSERT [dbo].[Vista] ([idVista], [Nombre], [idMenu]) VALUES (6, N'Nuevo', 2)
SET IDENTITY_INSERT [dbo].[Vista] OFF
GO
ALTER TABLE [dbo].[BodegaProducto]  WITH CHECK ADD  CONSTRAINT [FK_BodegaProducto_Bodega] FOREIGN KEY([idBodega])
REFERENCES [dbo].[Bodega] ([idBodega])
GO
ALTER TABLE [dbo].[BodegaProducto] CHECK CONSTRAINT [FK_BodegaProducto_Bodega]
GO
ALTER TABLE [dbo].[BodegaProducto]  WITH CHECK ADD  CONSTRAINT [FK_BodegaProducto_Producto] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Producto] ([idProducto])
GO
ALTER TABLE [dbo].[BodegaProducto] CHECK CONSTRAINT [FK_BodegaProducto_Producto]
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Asesor] FOREIGN KEY([idAsesor])
REFERENCES [dbo].[Asesor] ([idAsesor])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Asesor]
GO
ALTER TABLE [dbo].[Orden]  WITH CHECK ADD  CONSTRAINT [FK_ord_cli] FOREIGN KEY([idCliente])
REFERENCES [dbo].[Cliente] ([idCliente])
GO
ALTER TABLE [dbo].[Orden] CHECK CONSTRAINT [FK_ord_cli]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_prod_cat] FOREIGN KEY([idCategoria])
REFERENCES [dbo].[Categoria] ([idCategoria])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_prod_cat]
GO
ALTER TABLE [dbo].[Rol_Vista]  WITH CHECK ADD  CONSTRAINT [FK_rol_menu] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[Rol_Vista] CHECK CONSTRAINT [FK_rol_menu]
GO
ALTER TABLE [dbo].[Rol_Vista]  WITH CHECK ADD  CONSTRAINT [FK_Rol_Vista_Vista] FOREIGN KEY([idVista])
REFERENCES [dbo].[Vista] ([idVista])
GO
ALTER TABLE [dbo].[Rol_Vista] CHECK CONSTRAINT [FK_Rol_Vista_Vista]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_rol_usu] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_rol_usu]
GO
ALTER TABLE [dbo].[Vista]  WITH CHECK ADD  CONSTRAINT [FK_Vista_Menu] FOREIGN KEY([idMenu])
REFERENCES [dbo].[Menu] ([idMenu])
GO
ALTER TABLE [dbo].[Vista] CHECK CONSTRAINT [FK_Vista_Menu]
GO
USE [master]
GO
ALTER DATABASE [DBORDEN] SET  READ_WRITE 
GO
