﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class DetalleOrdenViewModel
    {
        public int IdDetalle { get; set; }
        public int IdCliente { get; set; }
        public int Consecutivo { get; set; }
        public int IdProducto { get; set; }
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public decimal? Descuento { get; set; }
        public decimal Total { get; set; }
        public virtual Producto Producto { get; set; }
    }
}