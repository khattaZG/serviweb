﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class CiudadViewModel
    {
        public int IdCiudad { get; set; }
        public string Nombre { get; set; }
    }
}