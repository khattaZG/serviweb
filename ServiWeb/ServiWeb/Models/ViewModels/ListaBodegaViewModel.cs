﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class ListaBodegaViewModel
    {
        public int IdBodega { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}