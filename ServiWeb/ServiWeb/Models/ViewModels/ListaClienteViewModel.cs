﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class ListaClienteViewModel
    {
        public int IdCliente { get; set; }
        public string Nitcc { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Correo { get; set; }
        public string Contacto { get; set; }
        public string CorreoContacto { get; set; }
        public string TelContacto { get; set; }
        public int IdAsesor { get; set; }
        public int IdCiudad { get; set; }
        public string Ciudad { get; set; }
        public List<int> ListClienteSelected { get; set; }


    }
}