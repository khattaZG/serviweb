﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Models.ViewModels
{
    public class ClienteViewModel
    {
        [Required(ErrorMessage = "Nit es requerido")]
        [StringLength(20)]
        public string Nitcc { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(200)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Dirección es requerido")]
        [StringLength(200)]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "Telefono es requerido")]
        [StringLength(50)]
        public string Telefono1 { get; set; }

        public string Telefono2 { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Correo { get; set; }

        public string Contacto { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string CorreoContacto { get; set; }

        public string TelContacto { get; set; }

        [Required(ErrorMessage = "Ciudad es requerido")]
        [StringLength(200)]
        public string Ciudad { get; set; }

        public int IdCiudad { get; set; }

        public string NombreCiudad { get; set; }

        [Required(ErrorMessage = "Asesor es requerido")]
        public int IdAsesor { get; set; }

        public virtual Asesor Asesor { get; set; }
    }
}