﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class OrdenViewModel
    {
        public int IdOrden { get; set; }
        [Required]      
        [Display(Name = "Cliente")]
        public int IdCliente { get; set; }
        public DateTime Fecha { get; set; }
        public string Nitcc { get; set; }
        public string Nombre { get; set; }
        public string Busqueda { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public decimal Dcto { get; set; }
        public decimal VrIva { get; set; }
        public string Estado { get; set; }
        public DetalleOrden DetalleOrden { get; set; }
        public List<OrdenTemporalListModel> OrdenTemporalListModel { get; set; }
        public List<ListaProductosTemporales> ListaProductosTemporales { get; set; }
        public List<ListaProductoConfirmadoViewModel> ListaProductoConfirmadoViewModel { get; set; }

    }
}