﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ServiWeb.Models.ViewModels
{
    public class OperarioViewModel
    {
        [Required]
        [StringLength(200)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        public bool Estado { get; set; }
    }
}