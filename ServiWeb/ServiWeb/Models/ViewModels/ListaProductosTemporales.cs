﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class ListaProductosTemporales
    {
        public int IdProductoTemporal { get; set; }
        public int IdProducto { get; set; }
        public int IdBodegaProducto { get; set; }
        public decimal? Precio { get; set; }
        public int Cantidad { get; set; }
        public string Descripcion { get; set; }
        public int IdCliente { get; set; }
        public decimal Dcto { get; set; }
        public decimal? TotalProducto { get; set; }
        public bool Estado { get; set; }
    }
}