﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class OrdenTemporalListModel
    {
        public bool Selected { get; set; }
        public int IdTemOrden { get; set; }
        public int IdCliente { get; set; }
        public DateTime Fecha { get; set; }
        public int IdProducto { get; set; }
        public int IdBodegaProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Dcto { get; set; }
        public int? Consecutivo { get; set; }
        public int? TotalProducto { get; set; }
        public bool Estado { get; set; }
        ///Propiedades Adicionales
        public string Nitcc { get; set; }
        public string NombreCliente { get; set; }

        public string Referencia { get; set; }
        public string Descripcion { get; set; }
        public decimal? PrecioVenta { get; set; }
        public decimal? TotalUnidad { get; set; }

        public int IdBodega { get; set; }
        public string NombreBodega { get; set; }
        public int Existencia { get; set; }
        public virtual TemOrden TemOrdenViewModel { get; set; }

    }
}