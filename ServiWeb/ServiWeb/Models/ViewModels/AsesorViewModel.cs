﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class AsesorViewModel
    {

        public AsesorViewModel()
        {
            Cliente = new HashSet<Cliente>();
        }
        [Required]
        [StringLength(200)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        public bool Estado { get; set; }

        public virtual ICollection<Cliente> Cliente { get; set; }
    }
}