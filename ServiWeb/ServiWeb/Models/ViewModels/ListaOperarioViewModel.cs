﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class ListaOperarioViewModel
    {
        public int IdOperario { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
    }
}