﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class ProductoViewModel
    {
        public int IdProducto { get; set; }
        public string Referencia { get; set; }
        public string Descripcion { get; set; }
        public string Marca { get; set; }
        public string UnidadMedida { get; set; }
        public string Dimension { get; set; }
        public decimal PrecioCompra { get; set; }
        public decimal PrecioVenta { get; set; }
        public int IdCategoria { get; set; }

        public virtual Bodega Bodega { get; set; }
        public virtual Categoria Categoria { get; set; }
    }
}