﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiWeb.Models.ViewModels
{
    public class BodegaProductoViewModel
    {
        public int IdBodegäProducto { get; set; }
        public int IdBodega { get; set; }
        public string Referencia { get; set; }
        public int Existencia { get; set; }

        public virtual Producto Producto { get; set; }

        public virtual Bodega Bodega { get; set; }
    }
}