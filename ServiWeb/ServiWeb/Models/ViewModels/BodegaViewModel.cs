﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ServiWeb.Models.ViewModels
{
    public class BodegaViewModel
    {
        [Required]
        [StringLength(400)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}