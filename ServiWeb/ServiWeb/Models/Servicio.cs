//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Servicio
    {
        public int idServicio { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public decimal comision { get; set; }
        public bool estado { get; set; }
        public bool cobro { get; set; }
    }
}
