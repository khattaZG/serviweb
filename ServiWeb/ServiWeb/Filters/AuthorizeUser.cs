﻿using ServiWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple =false)]
    public class AuthorizeUser : AuthorizeAttribute
    {
        private Usuario oUsuario;
        private DBORDENEntities1 db = new DBORDENEntities1();
        private int idVista;

        public AuthorizeUser(int idVista = 0)
        {
            this.idVista = idVista;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            String nombreOperacion = "";
            String nombreModulo = "";
            try
            {
                oUsuario = (Usuario)HttpContext.Current.Session["User"];
                var lstMisVistas = from m in db.Rol_Vista
                          where m.idRol == oUsuario.idRol
                              && m.idVista == idVista
                                   select m;


                if (lstMisVistas.ToList().Count() ==0)
                {
                    var oVista = db.Vista.Find(idVista);
                    int? idMenu = oVista.idMenu;
                    nombreOperacion = getNombreDeOperacion(idVista);
                    nombreModulo = getNombreDelModulo(idMenu);
                    filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?Vista=" + nombreOperacion + "&Menu=" + nombreModulo + "&msjeErrorExcepcion=");
                }
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?Vista=" + nombreOperacion + "&Menu=" + nombreModulo + "&msjeErrorExcepcion=" + ex.Message);
            }
        }

        public string getNombreDeOperacion(int idVista)
        {
            var ope = from op in db.Vista
                      where op.idVista == idVista
                      select op.Nombre;
            String nombreOperacion;
            try
            {
                nombreOperacion = ope.First();
            }
            catch (Exception)
            {
                nombreOperacion = "";
            }
            return nombreOperacion;
        }

        public string getNombreDelModulo(int? idMenu)
        {
            var menu = from m in db.Menu
                         where m.idMenu == idMenu
                         select m.nombre;
            String nombreModulo;
            try
            {
                nombreModulo = menu.First();
            }
            catch (Exception)
            {
                nombreModulo = "";
            }
            return nombreModulo;
        }

    }
}