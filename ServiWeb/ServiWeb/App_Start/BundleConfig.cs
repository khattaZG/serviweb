﻿using System.Web;
using System.Web.Optimization;

namespace ServiWeb
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //js
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                      "~/Scripts/jquery-ui-{version}.js"));
            ////css
            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                   "~/Content/themes/base/jquery-ui.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquerydp").Include(
            "~/Scripts/jquery.datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/jquerydpcss").Include(
                      "~/Content/jquery.datetimepicker.css"));

            bundles.Add(new StyleBundle("~/Content/jquerydttbl").Include(
                      "~/Content/jquery.dataTables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquerydt").Include(
                      "~/Scripts/jquery.dataTables.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/dtbootstrap4").Include(
            "~/Scripts/dataTables.bootstrap4.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/btstr").Include(
            "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/smrntext").Include(
            "~/Scripts/summernote-ext-rtl.js"));

            bundles.Add(new ScriptBundle("~/bundles/ctmeditor").Include(
            "~/Scripts/script-custom-editor.js"));
            bundles.Add(new ScriptBundle("~/bundles/smrnt").Include(
            "~/Scripts/summernote.js"));

            bundles.Add(new StyleBundle("~/Content/smrntcss").Include(
            "~/Content/summernote.css"));

            bundles.Add(new ScriptBundle("~/bundles/ppr").Include(
            "~/Scripts/umd/popper.js"));
            bundles.Add(new ScriptBundle("~/bundles/xlsx").Include(
            "~/Scripts/xlsx.full.min.js"));

            bundles.Add(new StyleBundle("~/Content/chosencss").Include(
                      "~/Content/chosen.css",
                      "~/Content/chosen.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                "~/Scripts/chosen.jquery.min.js",
                "~/Scripts/chosen.jquery.js",
                "~/Scripts/docsupport/prism.js",
                "~/Scripts/docsupport/init.js"));

            bundles.Add(new ScriptBundle("~/bundles/extendedcombo").Include(
"~/Scripts/extendedcombo.full.js"));
            bundles.Add(new StyleBundle("~/Content/extendedcombocss").Include(
"~/Content/extendedcombo.css"));

            // Boorstrap dropdown select
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-select").Include(
                                  "~/Scripts/bootstrap-select.js",
                                  "~/Scripts/script-bootstrap-select.js"));

            // Bootstrap dropdown select
            bundles.Add(new StyleBundle("~/Content/Bootstrap-Select/css").Include(
                                 "~/Content/bootstrap-select.css",
                                 "~/Content/bootstrap-select.min.css"));
        }
    }
}
