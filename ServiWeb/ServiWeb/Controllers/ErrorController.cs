﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Controllers
{
 
        public class ErrorController : Controller
        {
            // GET: Error
            [HttpGet]
            public ActionResult UnauthorizedOperation(String Vista, String Menu, String msjeErrorExcepcion)
            {
                ViewBag.Vista = Vista;
                ViewBag.Menu = Menu;
                ViewBag.msjeErrorExcepcion = msjeErrorExcepcion;
                return View();
            }
        }
    
}