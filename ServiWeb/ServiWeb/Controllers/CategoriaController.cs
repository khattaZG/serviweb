﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiWeb.Models;
using ServiWeb.Models.ViewModels;

namespace ServiWeb.Controllers
{
    public class CategoriaController : Controller
    {
        // GET: Categoria
        public ActionResult Index()
        {
            List<ListaCategoriaViewModel> lst;
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lst = (from d in db.Categoria
                       select new ListaCategoriaViewModel
                       {
                           IdCategoria = d.idCategoria,
                           Nombre = d.nombre,
                           Descripcion = d.descripcion
                       }).ToList();
            }
            return View(lst);
        }
        public ActionResult Nuevo()
        {
            return View();
        }
        // POST: Categoria
        [HttpPost]
        public ActionResult Nuevo(CategoriaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oCategoria = new Categoria();
                        oCategoria.nombre = model.Nombre.ToString().ToUpper();
                        oCategoria.descripcion = model.Descripcion;
                        db.Categoria.Add(oCategoria);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            ListaCategoriaViewModel model = new ListaCategoriaViewModel();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oCategoria = db.Categoria.Find(Id);
                model.Nombre = oCategoria.nombre;
                model.Descripcion = oCategoria.descripcion;
                model.IdCategoria = oCategoria.idCategoria;
            }
            return View(model);
        }

        // POST: Categoria
        [HttpPost]
        public ActionResult Editar(ListaCategoriaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oCategoria = db.Categoria.Find(model.IdCategoria);
                        oCategoria.nombre = model.Nombre.ToString().ToUpper();
                        oCategoria.descripcion = model.Descripcion;
                        db.Entry(oCategoria).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Eliminar(int Id)
        {
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oCategoria = db.Categoria.Find(Id);
                db.Categoria.Remove(oCategoria);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}