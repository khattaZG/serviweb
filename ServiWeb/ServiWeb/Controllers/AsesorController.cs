﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiWeb.Models;
using ServiWeb.Models.ViewModels;
namespace ServiWeb.Controllers
{
    public class AsesorController : Controller
    {
        // GET: Asesor
        public ActionResult Index()
        {
            List<ListaAsesorViewModel> lst;
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lst = (from d in db.Asesor
                            select new ListaAsesorViewModel
                            {
                                IdAsesor = d.idAsesor,
                                Nombre = d.nombre.ToUpper(),
                                Estado = d.estado
                            }).ToList();
            }
            return View(lst);
        }


        public ActionResult Nuevo()
        {
            return View();
        }

        // POST: Asesor
        [HttpPost]
        public ActionResult Nuevo(AsesorViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oAsesor = new Asesor();
                        oAsesor.nombre = model.Nombre.ToString().ToUpper();
                        oAsesor.estado = model.Estado;
                        db.Asesor.Add(oAsesor);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            ListaAsesorViewModel model = new ListaAsesorViewModel();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oAsesor = db.Asesor.Find(Id);
                model.Nombre = oAsesor.nombre;
                model.Estado = oAsesor.estado;
                model.IdAsesor = oAsesor.idAsesor;
            }
            return View(model);
        }

        // POST: Asesor
        [HttpPost]
        public ActionResult Editar(ListaAsesorViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oAsesor = db.Asesor.Find(model.IdAsesor);
                        oAsesor.nombre = model.Nombre.ToString().ToUpper();
                        oAsesor.estado = model.Estado;
                        db.Entry(oAsesor).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Eliminar(int Id)
        {
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oAsesor = db.Asesor.Find(Id);
                db.Asesor.Remove(oAsesor);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}