﻿using ServiWeb.Models;
using ServiWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto
        public ActionResult Index()
        {

            List<ListaProductoViewModel> lstProducto = new List<ListaProductoViewModel>();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lstProducto = (from d in db.Producto
                       select new ListaProductoViewModel
                       {
                            IdProducto = d.idProducto,
                            Referencia = d.referencia,
                            Descripcion = d.descripcion,
                            Marca = d.marca,
                            UnidadMedida = d.unidadMedida,
                            Dimension = d.dimension,
                            PrecioCompra = d.precioCompra,                        
                            PrecioVenta = d.precioVenta,
                            IdCategoria = d.idCategoria  
                       }
                       ).ToList();
            }
            return View(lstProducto);            
        }

       

        List<SelectListItem> lstBodegaProducto;

        public void llenarBodegaXProductos(int? id)
        {
                 
            using (var db = new DBORDENEntities1())
            {
                lstBodegaProducto = (from BodProd in db.BodegaProducto
                                     join Bodega in db.Bodega
                                     on BodProd.idBodega equals
                                     Bodega.idBodega
                                     where BodProd.idProducto == id
                               select new SelectListItem
                               {
                                   Text = Bodega.nombre,
                                   Value = Bodega.idBodega.ToString()
                               }).ToList();
                lstBodegaProducto.Insert(0, new SelectListItem { Text = "Seleccione....", Value = "" });
                ViewBag.lstBodegaProducto = lstBodegaProducto;                 
            }            
        }



        List<BodegaProductoViewModel> lstExistencias;
        public void CargarExistencia(int idProducto, int idBodega)        {
            
            using (var db = new DBORDENEntities1())
            {
                lstExistencias = (from BodProd in db.BodegaProducto                            
                                     where BodProd.idProducto == idProducto && BodProd.idBodega == idBodega
                                     select new BodegaProductoViewModel
                                     {
                                         IdBodegäProducto = BodProd.idBodegaProducto,
                                         IdBodega = BodProd.idBodega,
                                         Referencia = BodProd.referencia,
                                         Existencia = BodProd.existencia

                                     }).ToList();
                ViewBag.lstExistencias = lstExistencias;                 
            }
        }


        [HttpPost]
        public ActionResult BuscarProducto(ListaProductoViewModel model)
        {        

            return View();
        }
        
    }
}