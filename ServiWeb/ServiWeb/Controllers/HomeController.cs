﻿using ServiWeb.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Controllers
{
    public class HomeController : Controller
    {
        [AuthorizeUser( idVista:1)]
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeUser(idVista: 2)]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [AuthorizeUser(idVista: 3)]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}