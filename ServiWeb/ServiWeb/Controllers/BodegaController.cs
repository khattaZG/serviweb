﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiWeb.Models;
using ServiWeb.Models.ViewModels;

namespace ServiWeb.Controllers
{
    public class BodegaController : Controller
    {
        // GET: Bodega
        public ActionResult Index()
        {
            List<ListaBodegaViewModel> lst;
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lst = (from d in db.Bodega
                       select new ListaBodegaViewModel
                       {
                           IdBodega = d.idBodega,
                           Nombre = d.nombre,
                           Descripcion = d.descripcion
                       }).ToList();
            }
            return View(lst);
        }
        public ActionResult Nuevo()
        {
            return View();
        }
        // POST: Bodega
        [HttpPost]
        public ActionResult Nuevo(BodegaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oBodega = new Bodega();
                        oBodega.nombre = model.Nombre.ToString().ToUpper();
                        oBodega.descripcion = model.Descripcion;
                        db.Bodega.Add(oBodega);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            ListaBodegaViewModel model = new ListaBodegaViewModel();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oBodega = db.Bodega.Find(Id);
                model.Nombre = oBodega.nombre;
                model.Descripcion = oBodega.descripcion;
                model.IdBodega = oBodega.idBodega;
            }
            return View(model);
        }

        // POST: Bodega
        [HttpPost]
        public ActionResult Editar(ListaBodegaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oBodega = db.Bodega.Find(model.IdBodega);
                        oBodega.nombre = model.Nombre.ToString().ToUpper();
                        oBodega.descripcion = model.Descripcion;
                        db.Entry(oBodega).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Eliminar(int Id)
        {
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oBodega = db.Bodega.Find(Id);
                db.Bodega.Remove(oBodega);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}