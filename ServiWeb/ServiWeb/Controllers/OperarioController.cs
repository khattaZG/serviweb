﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiWeb.Models;
using ServiWeb.Models.ViewModels;

namespace ServiWeb.Controllers
{
    public class OperarioController : Controller
    {
        // GET: Operario
        public ActionResult Index()
        {
            List<ListaOperarioViewModel> lst;
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lst = (from d in db.Operario
                       select new ListaOperarioViewModel
                       {
                           IdOperario = d.idOperario,
                           Nombre = d.nombre,
                           Estado = d.estado
                       }).ToList();
            }
            return View(lst);
        }
        public ActionResult Nuevo()
        {
            return View();
        }
        // POST: Operario
        [HttpPost]
        public ActionResult Nuevo(OperarioViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oOperario = new Operario();
                        oOperario.nombre = model.Nombre.ToString().ToUpper();
                        oOperario.estado = model.Estado;
                        db.Operario.Add(oOperario);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            ListaOperarioViewModel model = new ListaOperarioViewModel();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oOperario = db.Operario.Find(Id);
                model.Nombre = oOperario.nombre;
                model.Estado = oOperario.estado;
                model.IdOperario = oOperario.idOperario;
            }
            return View(model);
        }

        // POST: Operario
        [HttpPost]
        public ActionResult Editar(ListaOperarioViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oOperario = db.Operario.Find(model.IdOperario);
                        oOperario.nombre = model.Nombre.ToString().ToUpper();
                        oOperario.estado = model.Estado;
                        db.Entry(oOperario).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Eliminar(int Id)
        {
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oOperario = db.Operario.Find(Id);
                db.Operario.Remove(oOperario);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}