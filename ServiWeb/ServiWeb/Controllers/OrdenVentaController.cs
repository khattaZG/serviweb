﻿using ServiWeb.Models;
using ServiWeb.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Controllers
{
    public class OrdenVentaController : Controller
    {
        // GET: OrdenVenta
        public ActionResult Index()
        {
            llenarListaClientes();
            ViewBag.listaClientes = listaCliente;
            return View(listaCliente);
        }

        public void PasarIdCliente(string value)
        {
            var id = value;
            Session["idCliente"] = id;
        }

        public ActionResult NuevaOrden(OrdenViewModel modelo)
        {
            var id = Session["idCliente"];
            if (id != null)
            {
                int ClienteId = Convert.ToInt32(id);
                try
                {
                    var date = DateTime.Now;
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        modelo = (from d in db.Cliente.Where(d => d.idCliente == ClienteId)
                                  select new OrdenViewModel
                                  {
                                      IdCliente = d.idCliente,
                                      Nitcc = d.nitcc,
                                      Nombre = d.nombre,
                                      Fecha = date
                                  }).FirstOrDefault();
                    }
                    return View(modelo);
                } catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            } else {
                TempData["testmsg"] = "Debe seleccionar un cliente antes de continuar";                
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult NuevaOrden(OrdenViewModel modelo, string operation)
        {
            List<ListaProductoConfirmadoViewModel> lstConfirmados = new List<ListaProductoConfirmadoViewModel>();
            List<OrdenTemporalListModel> temporales = new List<OrdenTemporalListModel>();
            
            var oTempo = new ProductoConfirmado();
            var oOrden = new Orden();
           
            var oDetalleOrden = new DetalleOrden();
            if (operation == "Confirmar Orden")
            {
                using (DBORDENEntities1 db = new DBORDENEntities1())
                {
                    if (modelo.OrdenTemporalListModel != null && modelo.SubTotal > 0)
                    {
                        foreach (var i in modelo.OrdenTemporalListModel)
                        {
                            //guardar en tabla productoConfirmado
                            oTempo.idProducto = i.IdProducto;
                            oTempo.idBodegaProducto = i.IdBodegaProducto;
                            oTempo.precio = i.PrecioVenta;
                            oTempo.cantidad = i.Cantidad;
                            oTempo.descripcion = i.Descripcion;
                            oTempo.idCliente = modelo.IdCliente;
                            oTempo.dcto = i.Dcto;
                            oTempo.estado = false;
                        }
                        db.ProductoConfirmado.Add(oTempo);
                        db.SaveChanges();
                        oOrden.idCliente = modelo.IdCliente;
                        oOrden.fecha = DateTime.Now;
                        oOrden.subTotal = oTempo.cantidad * oTempo.precio;
                        oOrden.total = oTempo.cantidad * oTempo.precio;
                        oOrden.estado = "Creada";
                        oOrden.numOrden = db.Consecutivo.Select(x => x.numOrden).FirstOrDefault();
                        oOrden.cerrada = true;
                        db.Orden.Add(oOrden);
                        db.SaveChanges();
                        int consecutivo = oOrden.numOrden;
                        int idOrden = oOrden.idOrden;

                        //// listar productos confirmados
                        lstConfirmados = (from pc in db.ProductoConfirmado
                                          where pc.idCliente == modelo.IdCliente && pc.estado == false
                                          select new ListaProductoConfirmadoViewModel
                                          {
                                              IdProducto = pc.idProducto,
                                              IdBodegaProducto = pc.idBodegaProducto,
                                              Precio = pc.precio,
                                              Cantidad = pc.cantidad,
                                              Descripcion = pc.descripcion,
                                              IdCliente = pc.idCliente,
                                              Dcto = pc.dcto,
                                              TotalProducto = pc.precio * pc.cantidad,
                                              Estado = pc.estado
                                          }).ToList();
                        modelo.ListaProductoConfirmadoViewModel = lstConfirmados;

                        //Guardar Detalle Orden
                        foreach (var v in lstConfirmados)
                        {
                            oDetalleOrden.idProducto = v.IdProducto;
                            oDetalleOrden.idBodegaProducto = v.IdBodegaProducto;
                            oDetalleOrden.cantidad = v.Cantidad;
                            oDetalleOrden.descuento = v.Dcto;
                            oDetalleOrden.total = v.Cantidad * v.Precio;
                            oDetalleOrden.numOrden = consecutivo;
                            oDetalleOrden.idOrden = idOrden;
                            db.DetalleOrden.Add(oDetalleOrden);
                            db.SaveChanges();
                        }
                    } else {
                        TempData["testmsg"] = "Debe seleccionar un producto y cantidad";
                    }
                }
            } else {
                
                if (modelo.OrdenTemporalListModel != null)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        foreach (var i in modelo.OrdenTemporalListModel)
                        {
                            if (i.Cantidad > 0)
                            {
                                //guardar en tabla productoConfirmado
                                oTempo.idProducto = i.IdProducto;
                                oTempo.idBodegaProducto = i.IdBodegaProducto;
                                oTempo.precio = i.PrecioVenta;
                                oTempo.cantidad = i.Cantidad;
                                oTempo.descripcion = i.Descripcion;
                                oTempo.idCliente = modelo.IdCliente;
                                oTempo.dcto = i.Dcto;
                                oTempo.estado = false;
                                db.ProductoConfirmado.Add(oTempo);
                                db.SaveChanges();
                            }
                        }
                        lstConfirmados = (from pc in db.ProductoConfirmado
                                          where pc.idCliente == modelo.IdCliente && pc.estado == false
                                          select new ListaProductoConfirmadoViewModel
                                          {
                                              IdProducto = pc.idProducto,
                                              IdBodegaProducto = pc.idBodegaProducto,
                                              Precio = pc.precio,
                                              Cantidad = pc.cantidad,
                                              Descripcion = pc.descripcion,
                                              IdCliente = pc.idCliente,
                                              Dcto = pc.dcto,
                                              TotalProducto = pc.precio * pc.cantidad,
                                              Estado = pc.estado
                                          }).ToList();

                        modelo.ListaProductoConfirmadoViewModel = lstConfirmados;
                    }
                }
                modelo.OrdenTemporalListModel = null;
                modelo.OrdenTemporalListModel = BuscarProductos(modelo.Busqueda);
            }              
            return View(modelo);
        }

        List<SelectListItem> listaCliente;
        public void llenarListaClientes()
        {
            int agente = Convert.ToInt32(Session["IdAgente"]);

            using (var db = new DBORDENEntities1())
            {
                listaCliente = (from Cliente in db.Cliente
                                where Cliente.idAsesor == agente
                                select new SelectListItem
                                {
                                    Text = Cliente.nombre,
                                    Value = Cliente.idCliente.ToString()
                                }).ToList();
                listaCliente.Insert(0, new SelectListItem { Text = "--Seleccione--", Value = "" });
                ViewBag.listaCliente = listaCliente;
            }
        }
                       
        public List<OrdenTemporalListModel> BuscarProductos(string Busqueda)
        {
            List<ListaProductoViewModel> lstGetProducto = new List<ListaProductoViewModel>();
            List<OrdenTemporalListModel> lstProducto = new List<OrdenTemporalListModel>();
            string busqueda = Busqueda;
            using (var db = new DBORDENEntities1())
            {
                if (busqueda == null)
                {
                    lstGetProducto = (from d in db.Producto
                                      select new ListaProductoViewModel
                                      {
                                          IdProducto = d.idProducto,
                                          Referencia = d.referencia,
                                          Descripcion = d.descripcion,
                                          PrecioVenta = d.precioVenta,
                                      }).ToList();
                } else {
                    lstGetProducto = (from d in db.Producto
                                      where d.referencia.Contains(busqueda) || d.descripcion.Contains(busqueda)
                                      select new ListaProductoViewModel
                                      {
                                          IdProducto = d.idProducto,
                                          Referencia = d.referencia,
                                          Descripcion = d.descripcion,
                                          PrecioVenta = d.precioVenta,
                                      }).ToList();
                }
                foreach (var item in lstGetProducto)
                {
                    List<OrdenTemporalListModel> lst = new List<OrdenTemporalListModel>();
                    lst = (from bp in db.BodegaProducto
                           join p in db.Producto
                           on bp.idProducto  equals p.idProducto
                           join b in db.Bodega
                           on bp.idBodega equals b.idBodega
                           where bp.idProducto == item.IdProducto
                           select new OrdenTemporalListModel
                           {
                               IdProducto = bp.idProducto,
                               Referencia = bp.referencia,
                               Descripcion = p.descripcion,
                               PrecioVenta = p.precioVenta,
                               IdBodegaProducto = bp.idBodegaProducto,
                               IdBodega = bp.idBodega,
                               NombreBodega = b.nombre,
                               Existencia = bp.existencia
                           }).ToList();
                    lstProducto.AddRange(lst);
                }
                return lstProducto;
            }            
        }
    }
}