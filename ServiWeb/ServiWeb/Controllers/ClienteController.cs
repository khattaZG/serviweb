﻿using ServiWeb.Models;
using ServiWeb.Models.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiWeb.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
            List<ListaClienteViewModel> lst;
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lst = (from d in db.Cliente
                       select new ListaClienteViewModel
                       {  
                           IdCliente = d.idCliente,
                           Nitcc = d.nitcc,
                           Nombre = d.nombre,
                           Direccion = d.direccion,
                           Telefono1 = d.telefono1,
                           Telefono2 = d.telefono2,
                           Correo = d.correo,
                           Contacto = d.contacto,
                           CorreoContacto = d.correoContacto,
                           TelContacto = d.telContacto,
                           IdAsesor = d.idAsesor,
                           Ciudad = d.ciudad                       
                       }
                       ).ToList();
            }
            return View(lst);
        }

        // GET: Cliente
        [HttpGet]
        public ActionResult Nuevo()
        {
            //Cargar Lista Asesor
            List<ClienteViewModel> lstAsesor = new List<ClienteViewModel>();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lstAsesor = (from d in db.Asesor.Where(d=> d.estado == true)
                       select new ClienteViewModel
                       {   IdAsesor = d.idAsesor,
                           Nombre = d.nombre
                       }).ToList();                
            }
            ViewData["IdAsesor"] = new SelectList(lstAsesor, "IdAsesor", "Nombre");

            //Cargar Lista Ciudad
            List<CiudadViewModel> ltsCiudad = new List<CiudadViewModel>();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                ltsCiudad = (from x in db.Ciudad
                    select new CiudadViewModel
                    {
                        IdCiudad = x.idCiudad,
                        Nombre = x.nombre                     
                    }).ToList();
            }
            ViewBag.IdCiudad = new SelectList(ltsCiudad, "IdCiudad", "Nombre");
            return View();
        }

        // POST: Cliente
        [HttpPost]
        public ActionResult Nuevo(ClienteViewModel model, FormCollection form)
        {
            try
            {
                int ciudadId = model.IdCiudad;
                string ciudad;
                using (DBORDENEntities1 db1 = new DBORDENEntities1())
                {
                    ciudad = db1.Ciudad.FirstOrDefault(x => x.idCiudad == ciudadId).nombre;
                }
                using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
     
                        var oCliente = new Cliente();
                        oCliente.nitcc = model.Nitcc;
                        oCliente.nombre = model.Nombre;
                        oCliente.direccion = model.Direccion;
                        oCliente.telefono1 = model.Telefono1;
                        oCliente.telefono2 = model.Telefono2;
                        oCliente.correo = model.Correo;
                        oCliente.contacto = model.Contacto;
                        oCliente.correoContacto = model.CorreoContacto;
                        oCliente.telContacto = model.TelContacto;
                        oCliente.idAsesor = model.IdAsesor;
                        oCliente.ciudad = ciudad;
                        db.Cliente.Add(oCliente);
                        db.SaveChanges();                        
                }
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            List<CiudadViewModel> ltsCiudad = new List<CiudadViewModel>();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                ltsCiudad = (from x in db.Ciudad
                             select new CiudadViewModel
                             {
                                 IdCiudad = x.idCiudad,
                                 Nombre = x.nombre
                             }).ToList();
            }
            ViewBag.IdCiudad = new SelectList(ltsCiudad, "IdCiudad", "Nombre");

            List<ClienteViewModel> lstAsesor = new List<ClienteViewModel>();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                lstAsesor = (from d in db.Asesor.Where(d => d.estado == true)
                             select new ClienteViewModel
                             {
                                 IdAsesor = d.idAsesor,
                                 Nombre = d.nombre
                             }).ToList();
            }
            ViewData["IdAsesor"] = new SelectList(lstAsesor, "IdAsesor", "Nombre");

            ListaClienteViewModel model = new ListaClienteViewModel();
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oCliente = db.Cliente.Find(Id);
                model.Nombre = oCliente.nombre;
                model.Direccion = oCliente.direccion;
                model.Telefono1 = oCliente.telefono1;
                model.Telefono2 = oCliente.telefono2;
                model.Correo = oCliente.correo;
                model.Contacto = oCliente.contacto;
                model.CorreoContacto = oCliente.correoContacto;
                model.TelContacto = oCliente.telContacto;
                model.IdAsesor = oCliente.idAsesor;
                model.Ciudad = oCliente.ciudad;
                model.IdCliente = oCliente.idCliente;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(ListaClienteViewModel model, FormCollection form)
        {
            try
            {
                int ciudadId = model.IdCiudad;
                string ciudad;
                using (DBORDENEntities1 db1 = new DBORDENEntities1())
                {
                    ciudad = db1.Ciudad.FirstOrDefault(x => x.idCiudad == ciudadId).nombre;
                }


                if (ModelState.IsValid)
                {
                    using (DBORDENEntities1 db = new DBORDENEntities1())
                    {
                        var oCliente = db.Cliente.Find(model.IdCliente);
                        oCliente.nombre = model.Nombre.ToString().ToUpper();
                        oCliente.direccion = model.Direccion.ToString().ToUpper();
                        oCliente.telefono1 = model.Telefono1.ToString();
                        oCliente.telefono2 = model.Telefono2;
                        oCliente.correo = model.Correo;
                        oCliente.contacto = model.Contacto;
                        oCliente.correoContacto = model.CorreoContacto;
                        oCliente.telContacto = model.TelContacto;
                        oCliente.idAsesor = model.IdAsesor;
                        oCliente.ciudad = ciudad;
                        db.Entry(oCliente).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Eliminar(int Id)
        {
            using (DBORDENEntities1 db = new DBORDENEntities1())
            {
                var oCliente = db.Cliente.Find(Id);
                db.Cliente.Remove(oCliente);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}